import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

dependencies {
    api(kotlin("stdlib-jdk8"))
    api(libs.okhttp)
    implementation(libs.gson)
    implementation(libs.slf4j.api)
    testImplementation(libs.dynatest)
}

val configureBintray = ext["mavenCentral"] as (artifactId: String) -> Unit
configureBintray("owm-city-finder-client")
