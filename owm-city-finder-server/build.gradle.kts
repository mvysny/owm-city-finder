plugins {
    application
}

dependencies {
    api(kotlin("stdlib-jdk8"))
    implementation(libs.javalin)
    implementation(libs.slf4j.simple)
    implementation(libs.gson)
    implementation(libs.lucene)
    implementation(project(":owm-city-finder-client"))
    testImplementation(libs.dynatest)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

application {
    mainClass = "com.gitlab.mvysny.owmcityfinder.server.MainKt"
}
