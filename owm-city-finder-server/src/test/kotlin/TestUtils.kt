package com.gitlab.mvysny.owmcityfinder.server

import com.github.mvysny.dynatest.DynaNodeGroup
import java.io.File
import kotlin.test.expect

fun DynaNodeGroup.usingApp() {
    beforeGroup {
        // ensure reproducible builds
        CityDatabase.delete()
        CityListJsonCache.cityListJsonGz = File("src/test/files/city.list.json.gz").absoluteFile
        expect(true, "${CityListJsonCache.cityListJsonGz} doesn't exist!") { CityListJsonCache.cityListJsonGz.exists() }
        CityListJsonCache.initCache()
        CityDatabase.index()
    }
    afterGroup { CityDatabase.delete() }
}
