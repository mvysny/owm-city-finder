# Allows you to run this app easily as a docker container.
#
# 1. Build the image with: docker build --no-cache -t mvysny/owm-city-finder-server:latest .
# 2. Run the image with: docker run --rm -ti -p25314:25314 mvysny/owm-city-finder-server:latest
#
# Uses Docker Multi-stage builds: https://docs.docker.com/build/building/multi-stage/

# The "Build" stage. Copies the entire project into the container, into the /app/ folder, and builds it.
FROM eclipse-temurin:17 AS BUILD
COPY . /app/
WORKDIR /app/
RUN ./gradlew clean build -x test --no-daemon --info --stacktrace
WORKDIR /app/owm-city-finder-server/build/distributions/
RUN ls -la
RUN tar xvf owm-city-finder-server-*.tar && rm owm-city-finder-server-*.tar && rm owm-city-finder-server-*.zip
RUN mv owm-city-finder-server-* app
# At this point, we have the app (executable bash scrip plus a bunch of jars) in the
# /app/own-city-finder-server/build/distributions/app/ folder.

# The "Run" stage. Start with a clean image, and copy over just the app itself, omitting gradle, npm and any intermediate build files.
FROM eclipse-temurin:20
COPY --from=BUILD /app/owm-city-finder-server/build/distributions/app /app/
WORKDIR /app/bin
EXPOSE 25314
ENTRYPOINT ./owm-city-finder-server
